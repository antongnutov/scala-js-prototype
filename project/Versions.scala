object Versions {
  val http4s = "0.18.19"

  val scalaTags = "0.6.7"
  val scalaJsDom = "0.9.6"

  val config = "1.3.1"
  val log4j = "2.11.1"
  val scalatest = "3.0.5"
}