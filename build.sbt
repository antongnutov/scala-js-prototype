import NativePackagerHelper._
import sbt.Keys.crossTarget

organization in ThisBuild := "anton.gnutov"

version in ThisBuild := "0.1"

scalaVersion in ThisBuild := ScalaConfig.version

scalacOptions in ThisBuild := ScalaConfig.compilerOptions.value

val scalajsOutputDir = Def.settingKey[File]("directory for javascript files output by scalajs")

import sbtcrossproject.CrossPlugin.autoImport.crossProject

lazy val app =
  crossProject(JSPlatform, JVMPlatform)
    .in(file("./app"))
    .settings(
      name := "scala-js-prototype"
    )    
    .jvmSettings(

      libraryDependencies ++= Seq(
        "org.http4s" %% "http4s-dsl" % Versions.http4s,
        "org.http4s" %% "http4s-blaze-server" % Versions.http4s,

        "com.typesafe" % "config" % Versions.config,

        "org.apache.logging.log4j" % "log4j-api" % Versions.log4j,
        "org.apache.logging.log4j" % "log4j-slf4j-impl" % Versions.log4j,

        "org.scalatest" %% "scalatest" % Versions.scalatest
      ),

      imageNames in docker := {

        val gitLabRepository = s"registry.gitlab.com/antongnutov/scala-js-prototype"
        val defaultTag = "latest"
        // Tag the image with the value of the environment variable provided by GitLab continuous integration
        val imageTag = sys.env.getOrElse("CI_BUILD_REF", defaultTag)
        Seq(
          ImageName(
            repository = gitLabRepository,
            tag = Some(imageTag)
          )
        )
      },
      dockerfile in docker := {
        val appDir = stage.value
        val targetDir = "/opt/scala-js-app"
        new Dockerfile {
          from("openjdk:11-jre-slim")
          copy(appDir, targetDir)
          entryPoint(s"$targetDir/bin/${executableScriptName.value}")

          // We define all portforwarding in docker-compose.yml

          // Inside the docker container, our application will put a log file into this directory.
          // In docker-compose.yml, we use a volume to keep the logs between container invocations
          run("mkdir", "-p", "/opt/scala-js-app/logs")
        }
      },

      composeFile := s"${baseDirectory.value}/../../docker-compose.yml",

      // This way the Docker networks are removed using sbt-docker-compose. See https://github.com/Tapad/sbt-docker-compose/issues/39
      dockerMachineName := "scala-js-prototype-network",
      dockerImageCreationTask := docker.value,
  	
      mappings in (Compile, packageDoc) := Seq(),

  	  mappings in Universal ++= directory("webapp")

    )
    .enablePlugins(sbtdocker.DockerPlugin, DockerComposePlugin, JavaAppPackaging)
    .jsSettings(

      libraryDependencies ++= Seq(
        "org.scala-js" %%% "scalajs-dom" % Versions.scalaJsDom,
        "com.lihaoyi" %%% "scalatags" % Versions.scalaTags
      ),
      jsDependencies ++= Seq(),

      // Include the JavaScript dependencies
      skip in packageJSDependencies := false,

      // copy jsdeps into static folder
      crossTarget in(Compile, packageMinifiedJSDependencies) := scalajsOutputDir.value,

      // the same js filename for `fastOptJS` and `fullOptJS`
      artifactPath in Compile in fastOptJS := scalajsOutputDir.value / ((moduleName in fastOptJS).value + ".js"),
      artifactPath in Compile in fullOptJS := scalajsOutputDir.value / ((moduleName in fullOptJS).value + ".js"),

      scalaJSUseMainModuleInitializer := true
    )

lazy val client = app.js.settings(
  scalajsOutputDir := file("webapp") / "js",

  cleanFiles ++= Seq(scalajsOutputDir.value)
)

lazy val server = app.jvm.settings(
  bashScriptExtraDefines += """addJava "-Dconfig.file=${app_home}/../conf/app.conf"""",
  bashScriptExtraDefines += """addJava "-Dlog4j.configurationFile=${app_home}/../conf/log4j2.xml"""",
  bashScriptExtraDefines += """addJava "-server"""",
  bashScriptExtraDefines += """addJava "-Xmx256M""""
)
